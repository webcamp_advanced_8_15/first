<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Task".
 *
 * @property integer $taskId
 * @property string $name
 * @property string $details
 * @property string $createdDate
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['details'], 'string'],
            [['createdDate'], 'safe'],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taskId' => Yii::t('app', 'Task ID'),
            'name' => Yii::t('app', 'Name'),
            'details' => Yii::t('app', 'Details'),
            'createdDate' => Yii::t('app', 'Created Date'),
        ];
    }
}
